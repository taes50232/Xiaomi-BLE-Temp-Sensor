[![](https://img.shields.io/badge/https-taes50232.gitlab.io/Xiaomi_BLE_Temp_Sensor-green)](https://taes50232.gitlab.io/Xiaomi-BLE-Temp-Sensor)

## Pre-Alpha Project

|Sites| Progress | Status | Live |
|-----| ------ | ------ | ------ |
|Xiaomi-BLE-Temp-Sensor|![Prgress](https://img.shields.io/badge/progress-working-red)| [![pipeline status](https://gitlab.com/taes50232/Xiaomi-BLE-Temp-Sensor/badges/master/pipeline.svg)](https://gitlab.com/taes50232/Xiaomi-BLE-Temp-Sensor/pipelines)| [![](https://img.shields.io/badge/https-taes50232.gitlab.io/Xiaomi_BLE_Temp_Sensor-green)](https://taes50232.gitlab.io/Xiaomi-BLE-Temp-Sensor)|


## Plan for POC 
### Step 1: Setup a ubuntu 18.04 in localhost
### Step 2: Setup a python docker in ubuntu18.04

* "python3 -m venv my_env" 進入環境
* "source my_env/bin/activate" 激活環境
* "nano filename.py" 創建文件
* ctrl+x 退出，並按Y儲存
* "python filename.py" 執行文件

* "deactivate" 退出環境

### Step 2.5: Setup gitlab-runner in ubuntu18.04 so that gitlab-runner can download project code and run
### Step 3: 透過電腦取得小米藍芽溫溼度計資料

* 可以執行python語法的程式的工具 (Required)
* 擷取資料的技術且定期擷取 (Required)

### Step 4: In the python program, it can 整理資料並繪製圖表 (dashboard)

* How to have a good dashboard can publish the data (Required)
* 有檔案紀錄當前資料 (Required)
* 呈現所有資料 (Required)

### Step 5: Move the docker from ubuntu to rasberry pi
### Step 5.1: 讓樹莓派連上wifi，取得IP位址
### Step 5.3: Install gitlab-runner and let gitlab-runner download code from gitlab
### Step 5.3: 透過電腦讓樹莓安裝上python docker
### Step 5.4: 將寫好的程式放入樹莓

### Step 6: Make the docker always run when the rasberry pi is boot

見參考資料
* 在 /etc/init.d 資料匣建立一個指令檔，檔案名稱mypython
* 新增檔案權限－可執行
* 設定開機啟動
* 欲測試則使用指令 sudo service mypython start，並重新開機，再用指令 ps -ax|grep python 檢查程式是否有自動執行

## References

* [material design palette](https://www.materialpalette.com/)
* [藍芽溫濕度計使用](https://ithelp.ithome.com.tw/articles/10224376?sc=rss.iron)
* [ubuntu安裝python](https://www.digitalocean.com/community/tutorials/ubuntu-18-04-python-3-zh)
* [樹莓派安裝python](https://blog.csdn.net/panwen1111/article/details/88363771)
* [樹莓派設定自動開機執行python程式](https://medium.com/@chungyi410/%E5%9C%A8%E6%A8%B9%E8%8E%93%E6%B4%BE-raspberry-pi-%E4%B8%8A%E8%A8%AD%E5%AE%9A%E9%96%8B%E6%A9%9F%E8%87%AA%E5%8B%95%E5%9F%B7%E8%A1%8C-python-%E7%A8%8B%E5%BC%8F-56ccf89cffc5)

